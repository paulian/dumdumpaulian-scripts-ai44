#!/bin/bash
read -p  "What is Last Name? : " lname
read -p  "What is First Name? : " fname
read -p  "When is your Birthday? (YYYY-MM-DD) : " bday
MAX=$(printf '%s' $(( $(date -u -d"$(date +"%Y-%m-%d")" +%s) - $(date -u -d"$bday" +%s))))
echo "Hello, ${lname^} ${fname^}! You're $(((((($MAX/60)/60)/24)/364))) years old!"
#!/usr/bin/env bash
read -p  "Please input folder name : " fname
read -p  "Please input desired number of folders : " fnum
until [[ i -eq $fnum+1 ]]; do
    if [[ i -ne 1 ]]; then
        mkdir "$fname$i"
    fi
i=$((i+1))
done